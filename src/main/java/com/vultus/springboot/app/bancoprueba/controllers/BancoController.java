package com.vultus.springboot.app.bancoprueba.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vultus.springboot.app.bancoprueba.models.entity.Banco;
import com.vultus.springboot.app.bancoprueba.models.service.IBancoService;

@RestController
public class BancoController {
	
	@Autowired
	private IBancoService bancoService;
	
	@GetMapping("/listar")
	public List<Banco> listar(){
		return bancoService.findAll();
	}
	
	@GetMapping("/banco/{clave}")
	public Banco nombre(@PathVariable Integer clave){
		return bancoService.findByClave(clave);
	}
}
